# Resume - Matthew Adams

## What?

I'm Matthew Adams. MatthewAdams.md is my resume. It can be compiled into various formats, as necessary.

## How?

Here's how I generate the different formats.

### HTML

I edit MatthewAdams.md in Emacs using [markdown-mode](https://jblevins.org/projects/markdown-mode/). This requires installing a markdown renderer, e.g.:

```
brew install markdown
```

To make `markdown-mode` use my custom css, I have this in my `.emacs`:

```
(setq markdown-css-paths '("style.css"))
```

With that set up, I can just use `markdown-mode`'s live export (`C-c C-c l`, by default), which causes the html to be generated on save.

I use [browser-sync](https://browsersync.io/) so I can keep the html open in a browser and watch it change as I update it.

### PDF

I currently just print from the browser and choose the save as pdf option, with the printer margins set to 0 (since the css takes care of the margins).

### .doc/.docx

I use [pandoc](https://pandoc.org/):

```
pandoc MatthewAdams.md -s -o MatthewAdams.docx
```

That doesn't produce the formatting I want - I still need to work on refining this process.

## Why?

I used LaTeX to build my resume for years and was sad to let it go. However, a few factors pushed me to make the switch:

* *File format requirements*: Job applications often require resumes to be submitted in plain text or even .doc/.docx. It is possible to compile from LaTeX to plain text, but I'd rather have plain text be the source document - it is closer to being pure content after all.
* *My atrophying LaTeX chops*: I used to write up papers and lab reports and even (with questionable justification) took notes in class using LaTeX, but now I can get much more done much more quickly using CSS.
* *Ease of reading*: I find it easier to read/write content with minimal markup distracting me from the actual content.
* *Default appearance*: Many people probably associate the "LaTeX look" with respectability, formality, and academia but not so much with being efficient and cutting-edge.
* *Learning new skills*: Switching was a nice opportunity to mess around with compiling and styling markdown.

## When?

As needed, I suppose?

## Where?

Here.

## Who?

[Matthew Adams](http://matthew-adams.net/). See the resume for more details.

## Wherefore?

See "Why?"
