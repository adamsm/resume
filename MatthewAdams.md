# Matthew Adams *Software Engineer*

* [matthew-adams.net](http://www.matthew-adams.net)
* [matthewcodes@gmail.com](mailto:matthewcodes@gmail.com)
* [github.com/adamsmatthew](https://github.com/adamsmatthew)
* (865) 599-6417

## Experience

### Software Engineer - Wolfram Research *Dec. 2016 - Present*
* Leading development of the next-generation graphics rendering system for the Wolfram Cloud.
* Working directly with the CEO and customers on new and experimental graphics features.
* Mentoring interns and newer team members.

### Visualization Intern - Wolfram Research *May 2015 - Dec. 2016*
* Created a highly-efficient JavaScript framework for interactive plot effects.
* Worked on front-end graphics features and bugs in the Wolfram Cloud.

### Software Engineering Intern - MaxPoint Interactive *Summer 2014*
* Improved the service that schedules jobs to run on MaxPoint's clusters.

### Computer Science Teaching Assistant - University of North Carolina *Fall 2013, Spring 2014, Spring 2016*
* Prepared and gave lectures, graded, and held office hours for multiple classes.

### PyLearn Developer - Carleton College *Sept. 2012 - June 2013*
* Took a leadership role on the team that built PyLearn - a system that logs and improves Python's error messages - which was delivered ahead-of-schedule and quickly became popular with students.

### Physics/Astronomy Research Programmer - Carleton College *Summer 2011*
* Developed software - including numerical integrators, curve fitting tools, and graphing utilities - that helps astronomers better understand the accuracy of orbital motion approximations.

## Education
### Master of Science in Computer Science *2013 - 2016*
University of North Carolina - Chapel Hill, NC

### Bachelor of Arts in Physics and Computer Science, *magna cum laude* *2009 - 2013*
Carleton College - Northfield, MN

## Skills
* Primary: JavaScript (ES6+), React, Flow, WebGL, Node.js, Webpack, Jest, CSS, HTML, SVG, Wolfram Language (Mathematica)
* Secondary: Java, WebAssembly (Emscripten), Python, C, SQL
* Tools: Git, JIRA, Emacs
* Design: Sketch, Illustrator, Photoshop, Blender
* Other: French (Conversational), Audio Engineering
